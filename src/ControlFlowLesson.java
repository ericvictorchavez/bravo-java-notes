import java.util.Scanner;

public class ControlFlowLesson {
    public static void main(String[] args) {
//        COMPARISON OPERATORS
//                !=, <, >, <=, >=, ==

//        System.out.println(5 != 5);//False

//        System.out.println(5 <= 5);//True
//        System.out.println(5 >= 5);//True

//        !=, <, >, <=, >=, ==

//        LOGICAL OPERATORS
//        ||, &&
//        System.out.println(5 == 6 && 2>1 && 3!=3);//False
//        System.out.println(5 != 6 && 2>1 && 3==3);//TRUE
//
//        System.out.println(5 == 5 || 3 != 3);

//        IF STATEMENTS
        /*
        syntax:
        if (conditional 1 is met) {
            do task 1
        } else if ( conditional 2 is met) {
            do task 2
            }
            else {
            do task 3
            }
         */


//        int score = 34;
//        if(score >= 50) {
//            System.out.println("New High Score!");
//        } else {
//            System.out.println("Try again...");
//        }


//        STRING COMPARISON
//        Scanner sc = new Scanner(System.in);
//        System.out.print("Would you like to continue? [y/N]");
//        String userInput = sc.next();

//        DON'T DO THIS
//        boolean confirm = userInput == "y";

//        INSTEAD DO THIS
//        boolean confirm = userInput.equals("y");
//        if (confirm == true) {
//        System.out.println("Welcome to the Jungle");
//        } else if (userInput.equals("n")) {
//            System.out.println("Welcome to the Jungle ... you enter a lowercase n");
//        } else {
//            System.out.println("Bye bye bye");
//        }

        //DO THIS
//        if (userInput.equals("y")) {
//            System.out.println("Welcome to the Jungle");
//        }



        //DON'T DO THIS
//          if (userInput == "y") {
//              System.out.println("Welcome to the Jungle");
//          }

//   SWITCH STATEMENT
        /*
        SYNTAX:
        switch (variable used for "switching") {
        case firstCase: do task A;
                        break;
        case lastCase: do task B;
                        break;
         */

//        Scanner input = new Scanner(System.in);
//        System.out.println("Enter your grade: ");
//        String userGrade = input.nextLine().toUpperCase();
//        switch (userGrade) {
//            case "A":
//                System.out.println("Distinction");
//                break;
//            case "B":
//                System.out.println("B Grade");
//                break;
//            case "C":
//                System.out.println("C Grade");
//                break;
//            case "D":
//                System.out.println("D Grade");
//                break;
//            default:
//                System.out.println("Fail...");
//        }

//        While Loop
//        SYNTAX
        /*
        while (conditional) {
            //loop
        }
        */

//        int i = 0;
//        while (i <= 10) {
//            System.out.println("i is " + i);
//        }

//        DO while loop
        /*
        do {
        // statements
        } while ( condition is true)
         */

//        do{
//            int i = 0;
//            System.out.println("You will see this!");
//        } while (false);

//        FOR LOOP
//        for (var i = 0; i <= 10; i++) {
//            System.out.println(i);
//        }

//        ESCAPE SEQUENCES - DEALING WITH STRINGS
//        System.out.println("Hello \nWorld");
//        System.out.println("Hello \tWorld");

    }
}
