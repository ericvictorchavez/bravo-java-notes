import java.util.Arrays;

public class StringMethodsPractic {
    public static void main(String[] args) {

//        int myLength = "Good afternoon, good evening, and good night";

//        Syntax. int length = stringName.length();

        String name = "Good afternoon, good evening, and good night";
        int nameLength = name.length();
        System.out.println("The length: " + name + " contains " + nameLength + " letters.");


        String Str = new String("Good afternoon, good evening, and good night");

        System.out.print("Return Value :" );
        System.out.println(Str.toUpperCase() );
        System.out.println(Str.toLowerCase() );

//        String firstSubstring = "Hello World".substring(6);
//        String firstSubstring = "Hello World".substring(3);
        String firstSubstring = "Hello World".substring(10);

        System.out.println(firstSubstring);
        //World
        //lo World
        //d

//        String message = "Good evening, how are you?
        String Substring = "Good evening, how are you?".substring(0,12);//Good evening
//        String Substring = "Good evening, how are you?".substring(14);//how are you?
        System.out.println(Substring);

//        String myChar = "San Antonio";
//        char results = myChar.charAt(0);
//        System.out.println(results);

//        String myChar = "San Antonio".substring(0,1);//S
//        System.out.println(myChar);

//        String myChar = "San Antonio".substring(4,5);//A
//        System.out.println(myChar);

        String myChar = "San Antonio".substring(7,8);//o
        System.out.println(myChar);

        String bravo = "Eric, Jonathan, Adrian, MaryAnn, Sandra, Henry";
        String[] splitBravo = bravo.split(", ");
        System.out.println(Arrays.toString(splitBravo));



        String m1 = "Hello, ";
        String m2 = "how are you?";
        String m3 = "I love Java!";
        System.out.println(m1 +   m2 + " " + m3);

        int result = 89;
        System.out.println("You scored" + " " + result + " " +  "marks for your test!" );
    }
}
