package CodeboundGymProject;

public class Member {

    private char memberType;
	private Integer memberID;
	private String name;
	private double fees;

    public Member(char memberType, Integer memberID, String name, double fees) {
        this.memberType = memberType;
        this.memberID = memberID;
        this.name = name;
        this.fees = fees;
    }

    public char getMemberType() {
        return memberType;
    }

    public void setMemberType(char memberType) {
        this.memberType = memberType;
    }

    public Integer getMemberID() {
        return memberID;
    }

    public void setMemberID(Integer memberID) {
        this.memberID = memberID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getFees() {
        return fees;
    }

    public void setFees(double fees) {
        this.fees = fees;
    }

    public String toString() {
        return "Member{" +
                "memberType=" + memberType + "\n" +
                ", memberId=" + memberID +
                ", name=" + name + "\n" +
                ", fees=" + fees +
                '}';
    }
}
