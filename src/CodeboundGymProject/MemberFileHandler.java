package CodeboundGymProject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;

public class MemberFileHandler {

    public LinkedList<Member> readFile() {
        LinkedList<Member> m = new LinkedList();
        String lineRead;
        String[] splitLine;
        Member mem;

        BufferedReader bufferedReader = null;


        try (BufferedReader reader = new BufferedReader(new FileReader("./codeboundGymData/members.csv")))
        lineRead = reader.readLine();
        while (lineRead != null) {
            {
                splitLine = lineRead.split(", ");
                if (splitLine[0].equals("S")) {
                    mem = new SingleClubMember('S', Integer.parseInt(splitLine[1]), splitLine[2], Double.parseDouble(splitLine[3]), Integer.parseInt(splitLine[4]));
                } else {
                    mem = new MultiClubMember('M', Integer.parseInt(splitLine[1]), splitLine[2], Double.parseDouble(splitLine[3]), Integer.parseInt(splitLine[4]));
                }
                m.add(mem);
                lineRead = reader.readLine();
            }
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
        return m;
    }
}
