package CodeboundGymProject;

public class SingleClubMember extends Member {

    private Integer club;


    public SingleClubMember(char memberType, Integer memberID, String name, double fees,Integer club) {

        super(memberType, memberID, name, fees);
        this.club = club;
    }


    public Integer getClub() {
        return club;
    }


    public void setClub(Integer club) {
        this.club = club;
    }


    @Override
    public String toString() {
        return super.toString() + "," + club;
    }


}
