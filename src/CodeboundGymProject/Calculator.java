package CodeboundGymProject;

public interface Calculator <T extends Number> {
    public abstract double calculateFees(T clubID);
}
