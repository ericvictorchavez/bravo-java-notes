package Collections;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Set;

public class CollectionsHashMapExercise {
    /*TODO:
        Create a new class called CollectionsHashMapExercise
        Create a program that will append a specified element to the end of a hash map.
        Create a program that will iterate through all the elements in a hash map
        Create a program to test if a hash map is empty or not.
        Create a program to get the value of a specified key in a map
        Create a program to test if a map contains a mapping for the specified key.
        Create a program to test if a map contains a mapping for the specified value.
        Create a program to get a set view of the keys in a map
        Create a program to get a collection view of the VALUES contained in a map.
*/
    public static void main(String[] args) {

//        HashMap<String, String> country = new LinkedHashMap<>();
//        country.put("England", "London");
//        country.put("Ireland", "Dublin");
//        country.put("Italy", "Rome");
//        country.put("Germany", "Munich");
//        country.put("United States", "San Antonio");

        //Create a program that will append a specified element to the end of a hash map.
        //replace value providing value and new key which you wish to replace old
//        country.replace("United States", "America");
//
//        System.out.println(country);
//        country.replace("San Antonio", "HomeOfTheBrave");
//
//        country.put("Morocco", "Marrakesh");
//        System.out.println(country);


        // Create a program that will iterate through all the elements in a hash map

//        for(String i : country.keySet()){
//            System.out.println("Key : " + i + "\nValue: " + country.get(i));
//        }

        //



//               Iterator<Integer> KeySetIterator = map.keySet().iterator();
//        while(KeySetIterator.hasNext()){
//            Integer key = KeySetIterator.next();
//
//            System.out.println("Key: " + key + "Value: " + map.get(key));



        //Create a program to test if a hash map is empty or not.
        //Adrian's SOLUTION
//        System.out.println(country.isEmpty());

        //Lesson SOLUTION

//        boolean result = country.isEmpty();
//        System.out.println("Is my HashMap empty? " + result);
//        country.clear();
//        result = country.isEmpty();
//        System.out.println("Is my HashMap empty now? " + true);

        // Create a program to get the value of a specified key in a map
//        System.out.println(country.get("England"));
//        String value = country.get("England");
//        System.out.println("Value for England is: " + value);




        //Create a program to test if a map contains a mapping for the specified key.

        HashMap<String, Integer> people = new HashMap<>();
        people.put("Jim", 20);
        people.put("John", 15);
        people.put("Jill", 10);
        people.put("Jane", 13);
        people.put("Jack", 19);
        // System.out.println(people.remove("John"));
        if(people.containsKey("John")){
            System.out.println("John is here as a key");
        } else {
            System.out.println("No Johnny B good here! ");
        }








        // Create a program to test if a map contains a mapping for the specified value.

        if(people.containsValue(20)){
            System.out.println("You have a value of ");
        }else {
            System.out.println("No value of such here...");
        }




        // Create a program to get a set view of the keys in a map
        Set keySet = people.keySet();
        System.out.println("Key set values are " + keySet);







        //Create a program to get a collection view of the VALUES contained in a map.

        System.out.println("Collection view of values: " + people.values());






    }







}
