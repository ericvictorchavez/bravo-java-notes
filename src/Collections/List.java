package Collections;

import java.util.*;

/**Being a Collection subtype all methods in the Collection interface are also available in the List interface.
 /** List is a child interface of collection
 If we want to represent a group of individual objects as a single entity where duplicates are allowed and insertion order preserved then we should go for list.*/
/**In list duplicates are allowed and insertion order is preserved
 Java List the elements in a List has an order, and the elements can be iterated in that order.
 Since List is an interface you need to instantiate a concrete implementation of the interface in order to use it. You can choose between the following List implementations in the Java Collections API:*/

//java.util.ArrayList
//java.util.LinkedList
//java.util.Vector
//java.util.Stack

public class List {
    public static void main(String[] args) {



        /**
         * You create a List instance by creating an instance of one of the classes that implements the List interface. Here are a few examples of how to create a List instance:
         */

        ArrayList a = new ArrayList();

        LinkedList b = new LinkedList();

        Vector c = new Vector();

        Stack d = new Stack();
/**Of these implementations, the ArrayList is the most commonly used*/
        /**
         * EXAMPLES OF CREATING AN ARRAY LIST
         */
//\\

//Here is how we can create array lists in Java:
//Here, Type indicates the type of an array list
// 1) ArrayList<Type> arrayList= new ArrayList<>();

        //Initialize an ArrayList Using asList()
        // 2)new ArrayList<>(Arrays.asList(("Cat", "Cow", "Dog"));

        //  We can also create array lists using the List interface. It's because the ArrayList class implements the List interface.
        //  3)List<String> list = new ArrayList<>();




        /**A wrapper class is a class that wraps a primitive data type. For example, the Integer class wraps the int type, the Float class wraps the float type, etc.
         * Note: We can not create array lists of primitive data types like int, float, char, etc. Instead, we have to use their corresponding wrapper class.*/

// 1) ArrayList<Type> arrayList= new ArrayList<>();
// create String type arraylist
        ArrayList<String> words = new ArrayList<>();
        // Add elements
        words.add("Dog");
        words.add("Cat");
        words.add("Horse");
        System.out.println("ArrayList: " + words);
        //We can also add elements to an array list using indexes. For example,
        words.add(3, "Mouse");
        words.add(4, "Sheep");
        words.add(5, "Zebra");
        System.out.println("ArrayList: " + words);

        // create Integer type arraylist, integers is the corresponding wrapper class of int type
        ArrayList<Integer> nums = new ArrayList<>();
        nums.add(1);
        nums.add(2);
        nums.add(3);
        nums.add(4);

        // 2)new ArrayList<>(Arrays.asList(("Cat", "Cow", "Dog"));

        //Here, we have first created an array of 3 elements
        ArrayList<String> groceries = new ArrayList<>(Arrays
                //Then, the asList() method is used to convert the array into an array list.
                .asList("Milk", "Chips", "Bread"));
        System.out.println("ArrayList: " + groceries);

        // Access elements of the array list
        String element = groceries.get(1);
        System.out.println("Accessed Element: " + element);






    }


//Linked list are linked with each other using pointers, each element of the linked list references to the next element of the linked list.
    //Each element in the LinkedList is called the Node. Each Node of the LinkedList contains two items: 1) Content of the element 2) Pointer/Address/Reference to the Next Node in the
    /** Note:Linked List by provide following features:
     1. Linked list allows dynamic memory allocation, which means memory allocation is done at the run time by the compiler and we do not need to mention the size of the list during linked list declaration.

     2. Linked list elements don’t need contiguous memory locations because elements are linked with each other using the reference part of the node that contains the address of the next node of the list.

     3. Insert and delete operations in the Linked list are not performance wise expensive because adding and deleting an element from the linked list does’t require element shifting, only the pointer of the previous and the next node requires change.*/
    /**
     * In the following example we are using add(), addFirst() and addLast() methods to add the elements at the desired locations in the LinkedList
     */
    public void linkedList() {

        LinkedList<String> list = new LinkedList<String>();

        //Adding elements to the Linked list
        list.add("Steve");
        list.add("Carl");
        list.add("Raj");

        //Adding an element to the first position
        list.addFirst("Negan");

        //Adding an element to the last position
        list.addLast("Rick");

        //Adding an element to the 3rd position
        list.add(2, "Glenn");

        //Iterating LinkedList
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

    }


    /**What is the difference between ArrayList and vector class? This is one question you might get asked in an interview..
     * 1) Vector class is thread safety(Java threads are objects like any other Java objects), only one thread can enter into Vector objects at any moment of time during execution. Whereas multiple thread can access ArrayList objects  simultaneously.
     * 2)Performance wise ArrayList has better, because vector is synchronized it waits for one object to lock to enter th next.
     * 3) Manually change size of vector using the setSize() method. In ArrayList you can't change the current size manually
     *
     * 4)Traversing ArrayList uses iterator, vector class has  a method called elements() which returns Enumeration object containing all elements of the vector. Where as ArrayList does not have such methods.
     *
     * 5)In ArrayList, you have to start searching for a particular element from the beginning of an Arralist. But in the Vector, you can start searching for a particular element from a particular position in a vector. This makes the search operation in Vector faster than in ArrayList.
     *
     * Is considered as Legacy code, because, it exist in Java before the introduction of Collection Framework. Earlier it was not a part of Collections. Later it has been included in Collections. But, the older methods of vector class have been retained as it is.*/

    /**
     * Vector class is often considered as obsolete or “Due for Deprecation” by many experienced Java developers. They always recommend and advise not to use Vector class in your code. They prefer using ArrayList over Vector class.
     * <p>
     * Vector class combines two features – “Re-sizable Array” and “Synchronization“. This makes poor design. Because, if you need just “Re-sizable Array” and you use Vector class for that, you will get “synchronized Resizable Array” not just re-sizable array. This may reduce the performance of your application. Therefore, instead of using Vector class, always use ArrayList class. You will have re-sizable array and whenever you want to make it synchronized, use Collections.SynchronizedList().
     */




    //You should use interface as type on the return type of method, type of arguments, etc..to take advantage of Polymorphism. If you use interface than in future if the new implementation is shipped, then you are not required to change your program. For example, an application written using list will work as expected whether you pass a LinkedList, Vector, or ArrayList because they implement List, they obey the contract exposed by List interface.
    //
    //The only difference comes in performance, which is actually one of the drivers for change. In short, if you program using interface, tomorrow if a better implementation of your interface is available then you can switch to that without making any further change on the client-side (part of the program which uses that interface).

    //  3)List<String> list = new ArrayList<>();


    //EXAMPLE CREATING ARRAYLIST USING NEW STACK

    public static class stackExample {

        public void getStack() {
            System.out.println(stack);
        }

        public ArrayList stack;

        public stackExample(ArrayList stack) {
            this.stack = stack;
            stack.add("Hey");

        }


        public void push(Object obj) {
            // Add obj to the stack.
            stack.add(obj);

        }


        public Object pop() {
            // Return and remove the top item from
            // the stack.  Throws an EmptyStackException
            // if there are no elements on the stack.
            if (stack.isEmpty())
                throw new EmptyStackException();
            return stack.remove(stack.size() - 1);
        }

        public boolean isEmpty() {
            // Test whether the stack is empty.
            return stack.isEmpty();

        }





    } // end class Stack

}

