package Collections;
import java.util.*;

public class CollectionsArraysExercise {

        /*TODO:
            Create a new class called CollectionsArraysExercise
            Create a program that will print out an array of integers.
            Create a program that will print out an array of strings
            Create a program that iterates through all elements in an array. The array should hold the names of everyone in Alpha Class
            Create a program to insert elements into the array list. This element should be added on the first element and last. You can use a previous array or create a new one.
            Create a program to remove the third element from an array list. You can create a new array or use a previous one.
            Create an array of Dog breeds. Create a program that will sort the array list.
            Create an array of cat breeds. Create a program that will search an element in the array List.
            Now create a program that will reverse the elements in the array.
 */

    public static void main(String[] args) {


//Create a program that will print out an array of integers.
//ArrayList<Integer> numbers = new ArrayList<>();
//numbers.add(1);
//numbers.add(2);
//numbers.add(3);
//numbers.add(4);
//numbers.add(5);
//        System.out.println(numbers);









// Create a program that will print out an array of strings

//ArrayList<String> colors1 = new ArrayList<>();
//colors1.add("blue");
//colors1.add("red");
//        System.out.println(colors1);
//
//
//
//
//
////Creating ArrayList using asList Method
//ArrayList<String> colors = new ArrayList<>(Arrays.asList(
//        "blue",
//        "yellow",
//        "Pink",
//        "Green"
//));
//        System.out.println(colors);

// Create a program that iterates through all elements in an array. The array should hold the names of everyone in BravoClass


//ArrayList<String> Bravo = new ArrayList<>();
//Bravo.add("MaryAnn");
//Bravo.add("Jonathan");
//Bravo.add("Sandra");
//Bravo.add("Adrian");
//Bravo.add("Eric");
//Bravo.add("Henry");
//for(String s : Bravo){
//    System.out.println(s);
//}

//        ArrayList<String> bravoStudentsArray = new ArrayList<>();
//        bravoStudentsArray.add("Adrian");
//        bravoStudentsArray.add("Jonathan");
//        bravoStudentsArray.add("MaryAnn");
//        bravoStudentsArray.add("Sandra");
//        bravoStudentsArray.add("Eric");
//        bravoStudentsArray.add("Henry");
//        Iterator<String> iterator = bravoStudentsArray.iterator();
//        while (iterator.hasNext()){
//            System.out.println(iterator.next());
//        }








// Create a program to insert elements into the array list. This element should be added on the first element and last. You can use a previous array or create a new one.
//ArrayList animals1 = new LinkedList<String>();
        ArrayList<String> animals = new ArrayList<>(Arrays.asList(
                "Monkey",
                "Giraffe",
                "Elephant",
                "Lemur",
                "Peacock"));
        {
//            animals.add(0, "Ferret");
//            System.out.println(animals);
//            animals.add(6, "Parrot");
//            System.out.println(animals);


// Create a program to remove the third element from an array list. You can create a new array or use a previous one.


            animals.remove(2);System.out.println(animals);


// Create an array of Dog breeds. Create a program that will sort the array list.

            ArrayList<String> cats = new ArrayList<>();
            cats.add("Pit Bull");
            cats.add("Labrador");
            cats.add("English Bulldog");
            cats.add("Maltese");
            cats.add("Great Dane");
//            System.out.println(cats);
            Collections.sort(cats);
            System.out.println(cats);










//Create an array of cat breeds. Create a program that will search an element in the array List.
            ArrayList<String> cats1 = new ArrayList<>();
            cats1.add("Tiger");
            cats1.add("Kitty");
            cats1.add("Kitten");
            cats1.add("Lion");
            cats1.add("Cheetah");

            if(cats1.contains("Tiger")){
                System.out.println("I am a Cat who is a Tiger!");
            }else {
                System.out.println("No cheetah here");
            }

//Now create a program that will reverse the elements in the array.


            Collections.reverse(cats1);
            System.out.println(cats1);


        }

    }

}
