package Collections;

import java.util.ArrayList;
import java.util.Collections;

class Student{
    int roll;
    String name;
}

public class collectionsLecture {
    /** -Is a data structure that can be used to group or collect, objects.
     /*Collections and there are two main types of collections used in Java these are:
     * ArrayList
     * HashMap
     * -Is a data structure that can be used to group or collect, objects.*/

    //ArrayList
    //represents an array that can be changed in size. All elements in an array must be objects and same data type.
    /** .size() returns the number of elements in the array
     * .add() adds an element to the collection at the specified index
     * .get() returns the element specified at the index
     * .indexOf returns the first found index of the given item. If given item not found return -1
     * .contain() checks to see if the arrayList contains the given element
     * .lastIndexOf() finds the last index of the given element
     * .isEmpty checks to see if the list is empty.
     * .remove() removes the first occurrence of an item or an item at given index.*/



    public static void main(String[] args) {
        //Examples
        ArrayList<String> list1 = new ArrayList<String>();//declared an array list and defined (new) give datatype.

        list1.add("Jane");
        list1.add("Jim");
        list1.add("Jill");
        list1.add("Jack");
        list1.remove("Jim");
        list1.add("Jeb");
        System.out.println("list of list1 is:" + list1);
//
//        Student s1 = new Student();
//        s1.roll = 32923;
//        s1.name = "June";
//        ArrayList list2 = new ArrayList();
//
//        list2.add("Jim");
//        list2.add(10);
//        list2.add(s1);
//        System.out.println("list of list 2 is: " + list2);
//
//
//        ArrayList<Integer> numbers1 = new ArrayList();
//
//        numbers1.add(1);//adding to the ArrayList element
//        numbers1.add(8);
//        numbers1.add(4);
//        numbers1.add(2);
//        numbers1.add(0);
//        numbers1.add(9);
//        numbers1.add(3);
////        numbers1.addAll(list2);
//        System.out.println(numbers1);
//
//        System.out.println(numbers1.get(2));//getting element at index of 2
//
//        System.out.println(numbers1.set(4,8));
//        System.out.println(numbers1);
//
//        System.out.println(numbers1.remove(0));//removing from first index
//        System.out.println(numbers1);
//
//        Collections.sort(numbers1);//sorting out arraylist with Java Util
//        System.out.println(numbers1);//printing out numbers after sort
//        System.out.println(numbers1.size());

        ArrayList<String> roasts = new ArrayList<>();
        roasts.add("medium");
        roasts.add("dark");
        roasts.add("light");
        roasts.add("raw");
        roasts.add("wellDone");
        System.out.println(roasts);//print out arrayList
        System.out.println(roasts.contains("bold"));//checking to see if ArrayList contains given value
        System.out.println(roasts.lastIndexOf("light"));//return the index of given value
        System.out.println(roasts.isEmpty());//checking if ArrayList is empty
        roasts.clear();
        System.out.println(roasts);



    }

}

