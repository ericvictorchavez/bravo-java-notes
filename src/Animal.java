public class Animal {
    String color;
    public void eat(){
        System.out.println("I am eating!");
    }
    public void sleep(){
        System.out.println("I am sleeping");
    }


    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.eat();

        Cat cat = new Cat();
        cat.sleep();
    }
}

class Dog extends Animal{
    String breed;
    public void bark(){}

}
class Cat extends Animal{
    int age;
    public void meow(){}

}
