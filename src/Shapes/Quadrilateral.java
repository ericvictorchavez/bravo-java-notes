package Shapes;

public abstract class Quadrilateral extends Shape implements Measurable {

    protected double length;

    protected double width;

    public Quadrilateral(double side, double side1) {
        super();
    }

    public void Quadrilaterals (double length, double width)
    {
        this.length = length;
        this.width = width;
    }

    public double getLength()
    {
        return length;
    }

    public double getWidth()
    {
        return width;
    }

    //Abstract methods for setting the length and the width as well
    public abstract void setLength(double length);

    public abstract void setWidth(double Width);

}
