package Shapes;

//
class Square extends Quadrilateral {


    public Square(double side) {
        super(side, side);
    }

    @Override
    public void setLength(double length) {

        this.length = length;

    }

    @Override
    public void setWidth(double width) {
        this.width = width;
    }

    @Override
    public double getPerimeter() {
        return (this.length * 4);
    }

    @Override
    public double getArea() {
        return Math.pow(this.length, 2);
    }
//
////        Square should define a constructor that accepts one argument, side,
//
//    public Square(double side) {
//// and calls the parent's constructor to set both the length and width to the value of side.
//
//        super(side,side);//rectangle constructor setting to the square constructor data type
//
//
//    }
//
//    //    In the Square class, override the getArea and getPerimeter methods with the following definitions for a square
////    perimeter = 4 x side
////    area = side ^ 2
//    public double getArea(){
//        return Math.pow(this.length, 2);
//    }
//
//    public double getPerimeter(){
//        return(this.length * 4);
//    }
//
//
}
//

