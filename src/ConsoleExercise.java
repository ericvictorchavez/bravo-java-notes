import java.text.DecimalFormat;
import java.util.Scanner;

public class ConsoleExercise {

    public static void main(String[] args) {

//        For the following exercises, create a new class named ConsoleExercise with a main method like the one in your HelloWorld class.

//        Copy this code into your main method:
//        double pi = 3.14159;
//        Write some java code that uses the variable pi to output the following:
//        The value of pi is approximately 3.14.

//   Don't change the value of the variable, instead, reference one of the links above and use System.out.format to accomplish this.
//
//   Explore the Scanner Class
//   Prompt a user to enter a integer and store that value in an int variable using the nextInt method.
//        What happens if you input something that is not an integer?
//   Prompt a user to enter 3 words and store each of them in a separate variable, then display them back, each on a newline.
//                What happens if you enter less than 3 words?
//                What happens if you enter more than 3 words?
//   Prompt a user to enter a sentence, then store that sentence in a String variable using the .next method, then display that sentence back to the user.
//        do you capture all of the words?
//   Rewrite the above example using the .nextLine method.
//   Calculate the perimeter and area of Code Bound's classrooms

//   Prompt the user to enter values of length and width of a classroom at Code Bound.

//   Use the .nextLine method to get user input and cast the resulting string to a numeric type.
//             •	Assume that the rooms are perfect rectangles.
//             •	Assume that the user will enter valid numeric data for length and width.

//                Display the area and perimeter of that classroom.

//   The area of a rectangle is equal to the length times the width, and the perimeter of a rectangle is equal to 2 times the length plus 2 times the width.
//   Bonuses
//            •	Accept decimal entries
//            •	Calculate the volume of the rooms in addition to the area and perimeter



//        My Code
//        double pi = 3.14159;
//        System.out.format("The value of " + pi + " is approximately 3.14" );

//
//        Adrian Code
//        double pi = 3.14159;
//        DecimalFormat dc = new DecimalFormat("#.00");
//        System.out.format(dc.format(pi));

//        Stephens Code
//        double pi = 3.14159;
//        System.out.println("The value of pi is approximately " + pi);
//        System.out.format("The vaule of pi is approximately %.2f\n",pi);




        Scanner scanner = new Scanner(System.in);

//        System.out.print("Enter a integer: ");
//        int integer = scanner.nextInt();
//        System.out.println(integer);


//        System.out.print("Enter 3 word: ");
//        String word1 = scanner.next();
//        String word2= scanner.next();
//        String word3= scanner.next();
//        System.out.println(word1 +"" + word2 + "" + word3);


//        System.out.println("Enter a sentence");
//        String userInput = scanner.nextLine();
//        System.out.println(userInput);


        System.out.println("What is the length and width of the classroom?");

        String length = scanner.nextLine();
        int l =Integer.parseInt(length);
//        double l = Double.parseDouble(length);// decimal
        String width = scanner.nextLine();
        int w =Integer.parseInt(width);
//        double w = Double.parseDouble(width);// decimal

        System.out.println("The perimeter of the classroom is " + ((2 * w) + (2 * l)));

        System.out.println("The area of the classroom is " + (w * l));

    }
}
