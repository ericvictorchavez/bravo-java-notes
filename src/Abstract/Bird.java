package Abstract;

public class Bird extends Animal{

    @Override
    public void eat(){
        System.out.println("I eat birds");
    }
}
