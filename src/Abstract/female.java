package Abstract;

public class female extends People {


    @Override
    public void sleep(String b) {
        System.out.println(b);
    }

    @Override
    public void eat() {
        System.out.println("I am eating");
    }

    @Override
    public void laugh() {
        System.out.println("I am laughing ...");
    }
}
