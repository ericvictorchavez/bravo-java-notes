package Abstract;

public class Pet extends Cat {

    public void change(){
        System.out.println("I am a pet and a cat.");
    }

    @Override
    public void eat() {
        System.out.println("I am a pet and a cat eating");
    }
}
