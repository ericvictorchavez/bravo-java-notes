public class Person {
    public static long worldPopulation = 7000000000078798989L;// class property
    public String firstName;
    public String lastName;

    public  String sayHello() {
        return String.format("Hello from %s %s %d" , firstName, lastName,worldPopulation);
    }

    public static void main(String[] args) {
        Person theBestDrummerAlive = new Person();
        theBestDrummerAlive.firstName = "Niel Hart";
        Person.worldPopulation += 1;//accessing static property

        Person ada = new Person();//Default
        ada.firstName = "ada";
        ada.lastName = "Butter";
        System.out.println(ada.sayHello() );


//        System.out.println(Math.PI);
    }
}

/**Example 1 from the top comment out previous code to work
 */

//    //creating instance variables
//    public String firstName;
//    public String lastName;
////instance method
//    public  String sayHello() {
//        return String.format("Hello from %s %s" , firstName, lastName);
//    }
//
//    public static void main(String[] args) {
//        Person rick = new Person();
//
//
//        rick.firstName = "Ricky";
//        rick.lastName = "Sanchez";
//        System.out.println(rick.sayHello());
//    }
//}
