import org.w3c.dom.ls.LSOutput;

import java.util.Scanner;

public class ArraysExercise {

    public static void main(String[] args) {

        String[] vowels = new String[6];
        vowels[0] = "A";
        vowels[1] = "E";
        vowels[2] = "I";
        vowels[3] = "O";
        vowels[4] = "U";

        System.out.println(vowels[0]);
        System.out.println(vowels[1]);
        System.out.println(vowels[2]);
        System.out.println(vowels[3]);
        System.out.println(vowels[4]);


        String[] vowel = {"A", "E", "I", "O", "U"};
        for (int i = 0; i < vowel.length; i++) {
            System.out.println(vowel[i]);
        }

//        //TODO: Take 5 integer inputs from a user and store them in an array. SOUT out the integers *Hint:Scanner
//        Scanner scanner = new Scanner(System.in);
//        int [] intArray = new int[5];
//        System.out.println("Enter five integers");
//        for (int i = 0; i < 5; i++){
//            int input = scanner.nextInt();
//            intArray[i] = input;
//            System.out.println(intArray[i]);
//        }
        Scanner scanner = new Scanner(System.in);
        int [] intUser = new int[5];
        System.out.println("Enter five integers");
        for (int i = 0; i < 5; i++) {
            int input = scanner.nextInt();
            intUser[i] = input;
            System.out.println(intUser[i]);

        }



        //create a 2d array
            String[][] a ={ {"Bob","Jake","Eric"},
                    {"Lake","Bake","Mike"},

        };

        System.out.println(a[0][0]);
        System.out.println(a[0][1]);
        System.out.println(a[0][2]);
        System.out.println(a[1][0]);
        System.out.println(a[1][1]);
        System.out.println(a[1][2]);

    }

}
