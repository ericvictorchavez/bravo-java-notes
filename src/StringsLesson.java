public class StringsLesson {
    public static void main(String[] args) {
        //Strings
//        String message = "Hello World";
//        System.out.println(message);

//        String anotherMessage;
//        anotherMessage = "Another message assigned!";
//        System.out.println(anotherMessage);

        //CONCATENATION
        String myName = "Hellp World, " + "my name is Eric";

//        System.out.println(myName);
//        System.out.println(message + " " + anotherMessage + " " + myName);

//        String message = "Hello World";
//
//        if (message.equals("Hello World")) {
//            System.out.println("Message is Correct");
//        }

        //STRING COMPARISON METHODS
        // .equals(), .equalsIgnoreCase(), startsWith(), endsWith()

//        String input = "Bravo Rocks!";
//        System.out.println(input.equals("Bravo Rocks!"));//true
//        System.out.println(input.equals("bravo rocks!"));//false
//
//        System.out.println(input.equalsIgnoreCase("Bravo rocks!"));//true
//        System.out.println(input.equalsIgnoreCase("Bravo ROCK!"));//false
//
//        System.out.println(input.startsWith("Bravo"));//true
//        System.out.println(input.startsWith("bravo"));//false
//
//        System.out.println(input.endsWith("Rocks!"));//true
//        System.out.println(input.endsWith("rocks!"));//false


    }
}
