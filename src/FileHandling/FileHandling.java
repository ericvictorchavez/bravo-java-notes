package FileHandling;
/*
File, BufferedReader, FileReader, BufferedWrite, and FileWriter from java.io package
how to import?
import java.io.*;
 */
import javax.annotation.processing.Filer;
import java.io.*;
public class FileHandling {
    public static void main(String[] args) {
        //Reading if there's a text file with a name...use the FileReader class
        /*
        FileReader reads the content of a file
        - needs to be wrapped by a BufferedReader object
         */
        //create a string variable
        String line;
        //create a BufferedReader object
        BufferedReader bufferedReader = null;
        //try-catch-finally
        try{
            bufferedReader = new BufferedReader(new FileReader("./movieQuotes/hamlet.txt"));
            line = bufferedReader.readLine();
            while (line != null){
                System.out.println(line);
                line = bufferedReader.readLine();
            }
        }
        catch (IOException e){
            System.out.println(e.getMessage());
        }
        finally {
            try {
                if (bufferedReader != null){
                    bufferedReader.close();
                }
            }
            catch (IOException e){
                System.out.println(e.getMessage());
            }
        }
        //writing to a text file
        //create a string variable
//        String text = "I'll be back.";
//        try (BufferedWriter writer = new BufferedWriter(new FileWriter("./movieQuotes/terminator.txt", true)))
//        {
//            writer.write(text);
//            writer.newLine();
//        }
//        catch (IOException e){
//            System.out.println(e.getMessage());
//        }
        //Overwriting a text file / files text
        // create a string variable
//        String overwriteText = "Listen to many, speak to a few";
//        try (BufferedWriter writer = new BufferedWriter(new FileWriter("./movieQuotes/hamlet.txt")))
//        {
//            writer.write(overwriteText);
//            writer.newLine();
//        }
//        catch (IOException e){
//            System.out.println(e.getMessage());
//        }
        //Renaming a text file
//        File oldFileName = new File("./movieQuotes/hamlet.txt"); // targeting the file we want to rename
//        File newFileName = new File("./movieQuotes/shakespeareHamlet.txt"); // Creating the name that we want
//
//        oldFileName.renameTo(newFileName);
        //Deleting a text file
        File newFileName = new File("./movieQuotes/shakespeareHamlet.txt"); // Creating the name that we want
        newFileName.delete(); // cli 'rm -r filename
    } // end of our main
} // end of class
