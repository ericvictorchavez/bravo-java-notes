public class MethodLesson {
    public static void main(String[] args) {
//        Methods - are a sequence of statements that perform a specific task.

        //Call my greeting()
//        greeting("Hello","bravo");

//        System.out.println(returnFive());

//        System.out.println(yelling("Hello world"));

        System.out.println(message("Newsletter", "REPLIED"));
//        System.out.println(message(210));

//        String s = "This is a string";
//        String changeMe = "Hello Bravo!";
//        String secString = "Hello CodeBound";
//        changeString(s);
//
//        System.out.println(secString);

        counting(5);

    }//end of main method

//    basic syntax for method:
//    public static returnType methodName(param1, param2,) {
        // we what the code to do.
//    }

//    public static String greeting(String name) {
//        return String.format("Hello %s!", name);
//    }

//    public static void greeting(String greet, String name) {
//        System.out.printf("%s, %s!\n", greet, name);
//    }

//    public static int returnFive(){
//        return 5;
//    }
//
//    public static String yelling(String s) {
//        return s.toUpperCase();
//    }



//    public - this is the visibility modifier
//    defines whether or not other classes can "see" this method.

//    static - the presence of this keyword defines that method
    //belongs to the class, as opposed to instances of it.

//    METHOD OVERLOADING
//    -Defining multiple methods with the same name,
    //but with different parameter type, parameter order,
    //or number of parameters

    public static  String message() {
        return "This is a example of methods";
    }

    public static String message(String memo) {
        return memo;
    }

    public static String message(int code) {
        return "Code: " + code + "message";
    }

    public static String message(String memo, String status){
        return memo + "\nStatus:" + status;
    }

//    PASSING PARAMETERS TO METHODS
//    public static void changeString(String s) {
//        s = "This is a string";
//    }

//    RECURSION - that aims to solve a problem by dividing it into small chunks

//    counting 5 to 1 using recursion
    public static  void counting(int num) {
        if (num <= 0) {
            System.out.println("All done!");
            return;
        }
        System.out.println(num);
        counting(num - 1);
    }
}//end of class
